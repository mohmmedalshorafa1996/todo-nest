import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { config } from 'dotenv';
import { ValidateInputPipe } from './cor/pipes/validate.pipe';
config();
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  app.useGlobalPipes(new ValidateInputPipe());
  await app.listen(configService.get('PORT'));
}
bootstrap();
