import {
  Injectable,
  ArgumentMetadata,
  BadRequestException,
  ValidationPipe,
  UnprocessableEntityException,
} from '@nestjs/common';

@Injectable()
export class ValidateInputPipe extends ValidationPipe {
  public async transform(value, metadata: ArgumentMetadata) {
    try {
      return await super.transform(value, metadata);
    } catch (e: any) {
      if (e instanceof BadRequestException) {
        throw new UnprocessableEntityException(this.handleError('error'));
      }
    }
  }

  private handleError(errors) {
    return errors.map((error) => error.constraints);
  }
}
