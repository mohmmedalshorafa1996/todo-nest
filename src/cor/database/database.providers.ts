import { Sequelize } from 'sequelize-typescript';
import { Todo } from '../tables/todo';

export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      let config;
      switch (process.env.NODE_ENV as string) {
        case 'development':
          config = {};
          break;
        case 'test':
          config = {};
          break;
        case 'production':
          config = {};
          break;
        default:
          config = {
            username: 'kakashii',
            password: process.env.DB_PASS,
            database: process.env.DB_NAME_DEVELOPMENT,
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            dialect: process.env.DB_DIALECT,
          };
      }
      const sequelize = new Sequelize(config);
      sequelize.addModels([Todo]);
      await sequelize.sync();
      return sequelize;
    },
  },
];
