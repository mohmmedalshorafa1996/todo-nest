export default interface ITodo {
  id: number;
  todo: string;
}
