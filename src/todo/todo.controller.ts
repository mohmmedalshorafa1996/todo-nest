import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Delete,
} from '@nestjs/common';
import ITodo from '../cor/interface/ITodo';
import { TodoService } from './todo.service';

@Controller('todos')
export class TodoController {
  constructor(private readonly todoService: TodoService) {}
  @Get()
  async findAll() {
    return await this.todoService.findAll();
  }
  @Post()
  async create(@Body() todo: ITodo) {
    return await this.todoService.create(todo);
  }
  @Put('/:id')
  async update(@Param('id') id: number, @Body() todo: ITodo) {
    return await this.todoService.update(id, todo);
  }
  @Delete('/:id')
  async destroy(@Param('id') id: number) {
    return await this.todoService.destroy(id);
  }
}
