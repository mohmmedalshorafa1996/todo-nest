import { Module } from '@nestjs/common';
import { todosProviders } from './todo.providers';
import { TodoController } from './todo.controller';
import { TodoService } from './todo.service';

@Module({
  providers: [TodoService, ...todosProviders],
  controllers: [TodoController],
})
export class TodoModule {}
