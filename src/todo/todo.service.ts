import { Injectable, Inject } from '@nestjs/common';
import { Todo } from '../cor/tables/todo';
import ITodo from '../cor/interface/ITodo';

@Injectable()
export class TodoService {
  constructor(
    @Inject('TODO_REPOSITORY')
    private readonly todoRepository: typeof Todo,
  ) {}
  async findAll() {
    return await this.todoRepository.findAll<Todo>();
  }
  async create(todo: ITodo) {
    return await this.todoRepository.create<any>(todo);
  }
  async update(id: number, todo: ITodo) {
    const updateTodo = await this.todoRepository.update(
      { ...todo },
      { where: { id } },
    );
    if (updateTodo[0] === 1) {
      return {
        id,
        ...todo,
      };
    } else {
      return {
        massage: 'this does not exist',
      };
    }
  }
  async destroy(id: number) {
    const updateTodo = await this.todoRepository.destroy({ where: { id } });
    console.log(updateTodo);

    if (updateTodo === 1) {
      return {
        massage: 'todo is deleted',
      };
    } else {
      return {
        massage: 'this does not exist',
      };
    }
  }
}
