import { Todo } from '../cor/tables/todo';

export const todosProviders = [
  {
    provide: 'TODO_REPOSITORY',
    useValue: Todo,
  },
];
